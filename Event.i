# 1 "Event.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "Event.c"
# 1 "yakk.h" 1



# 1 "yaku.h" 1
# 5 "yakk.h" 2
# 24 "yakk.h"
enum TaskState {T_BLOCKED, T_READY, T_RUNNING};
enum KernelState {K_BLOCKED, K_RUNNING};

typedef struct TCB {
 unsigned int eventMask;
 unsigned char waitMode;
 unsigned char priority;
 void* stackPointer;
 enum TaskState state;
 unsigned int delayCount;
 struct TCB* next;
 struct TCB* prev;
} TCB;

typedef struct TaskBlock {
 TCB TCBPool[10 +1];
 unsigned int nextFreeTCB;
} TaskBlock;

typedef struct PriorityQueue {
 TCB* head;
 TCB* tail;
 unsigned int size;
} PriorityQueue;

typedef struct DelayQueue {
 TCB* head;
 unsigned int size;
} DelayQueue;

typedef struct Semaphore {
 int value;
 PriorityQueue queue;
} YKSEM;

typedef struct SemBlock {
 YKSEM SemPool[10];
 unsigned int nextFreeSem;
} SemBlock;

typedef struct MessageQueue {
 void** messages;
 unsigned int currentSize;
 unsigned int maxSize;
 PriorityQueue queue;
} YKQ;

typedef struct MessageQueueBlock {
 YKQ QueuePool[10];
 unsigned int nextFreeQueue;
} MsgQueueBlock;

typedef struct Event {
 unsigned int mask;
 PriorityQueue queue;
} YKEVENT;

typedef struct EventBlock {
 YKEVENT EventPool[10];
 unsigned int newFreeEvent;
} EventBlock;


void YKInitialize(void);
void YKEnterMutex(void);
void YKExitMutex(void);
void YKIdleTask(void);
void YKNewTask(void (* task) (void), void *taskStack, unsigned char priority);
void YKDelayTask(unsigned count);
void YKEnterISR(void);
void YKExitISR(void);
unsigned int YKGetISRCallDepth(void);
void YKScheduler(void);
void YKDispatcher(TCB* readyTask);
void YKTickHandler(void);
YKSEM* YKSemCreate(int initialValue);
void YKSemPend(YKSEM* semaphore);
void YKSemPost(YKSEM* semaphore);
YKQ* YKQCreate(void** start, unsigned size);
void* YKQPend(YKQ* queue);
int YKQPost(YKQ* queue, void* msg);
YKEVENT* YKEventCreate(unsigned initialValue);
unsigned YKEventPend(YKEVENT* event, unsigned eventMask, int waitMode);
void YKEventSet(YKEVENT* event, unsigned eventMask);
void YKEventReset(YKEVENT* event, unsigned eventMask);
TCB* getNewTCB(void);
YKSEM* getNewSem(void);
YKQ* getNewQueue(void);
YKEVENT* getNewEvent(void);
void YKRun(void);

unsigned int getYKCtxSwCount();
unsigned int getYKIdelCount();
void setYKIdelCount(int value);
void setYKCtxSwCount(int value);
unsigned int getYKTickNum();
# 2 "Event.c" 2
# 1 "clib.h" 1






void print(char *string, int length);
void printNewLine(void);
void printChar(char c);
void printString(char *string);


void printInt(int val);
void printLong(long val);
void printUInt(unsigned val);
void printULong(unsigned long val);


void printByte(char val);
void printWord(int val);
void printDWord(long val);


void exit(unsigned char code);


void signalEOI(void);
# 3 "Event.c" 2
# 1 "PriorityQueue.h" 1





void initializePriorityQueue(PriorityQueue* queue);

void insertPriorityQueue(PriorityQueue* queue, TCB* tcb);

TCB* peekPriorityQueue(PriorityQueue* queue);

TCB* removePriorityQueue(PriorityQueue* queue);

void printPriorityQueue(PriorityQueue* queue);
# 4 "Event.c" 2

extern PriorityQueue readyQueue;

YKEVENT* YKEventCreate(unsigned initialValue) {

 YKEVENT* newEvent;

 newEvent = getNewEvent();
 if (newEvent == 0) exit(5);

 newEvent->mask = initialValue;
 initializePriorityQueue(&(newEvent->queue));

 return newEvent;

}

unsigned YKEventPend(YKEVENT* event, unsigned eventMask, int waitMode) {

 TCB* runningTask;

 if (waitMode == 0) {
  YKEnterMutex();
  if (event->mask & eventMask) {
   return event->mask;
  } else {
   runningTask = removePriorityQueue(&readyQueue);
   runningTask->state = T_BLOCKED;
   runningTask->eventMask = eventMask;
   runningTask->waitMode = waitMode;
   insertPriorityQueue((&(event->queue)), runningTask);
   YKExitMutex();
   asm("int 0x20");
   return event->mask;
  }
 } else if (waitMode == 1) {
  YKEnterMutex();
  if (event->mask & eventMask == eventMask) {
   return event->mask;
  } else {
   runningTask = removePriorityQueue(&readyQueue);
   runningTask->state = T_BLOCKED;
   runningTask->eventMask = eventMask;
   runningTask->waitMode = waitMode;
   insertPriorityQueue((&(event->queue)), runningTask);
   YKExitMutex();
   asm("int 0x20");
   return event->mask;
  }
 } else {
  exit(6);
 }

}

void YKEventSet(YKEVENT* event, unsigned eventMask) {

 TCB* temp;
 TCB* current;

 YKEnterMutex();
 event->mask = event->mask | eventMask;

 current = event->queue.head;
 while (current != 0) {
  if ((current->waitMode == 1 &&
   current->eventMask & event->mask == current->eventMask) ||
   (current->waitMode == 0 &&
   current->eventMask & event->mask)) {

   temp = current;
   current = current->next;
    if (temp->prev != 0) {
    temp->prev->next = temp->next;
   } else {
    event->queue.head = temp->next;
   }
   if (temp->next != 0) {
    temp->next->prev = temp->prev;
   } else {
    event->queue.tail = temp->prev;
   }
   temp->prev = 0;
   temp->next = 0;
   event->queue.size--;
   temp->state = T_READY;
   insertPriorityQueue(&(readyQueue), temp);
   current = current->next;
  } else {
   current = current->next;
  }
 }
 YKExitMutex();
 asm("int 0x20");
 return;

}

void YKEventReset(YKEVENT* event, unsigned eventMask) {

 event->mask = event->mask & (~eventMask);
 return;

}
