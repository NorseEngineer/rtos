#include "clib.h"
#include "yakk.h"
#include "DelayQueue.h"
#include "lab7defs.h"

extern unsigned int YKTickCounter;
extern char KeyBuffer;
extern YKEVENT* charEvent;
extern YKEVENT* numEvent;

void resetHandler() {
	exit(0);
}

void tickHandler() {

	unsigned int localCounter;

	tickClock();
	YKEnterMutex();
	localCounter = ++YKTickCounter;
	YKExitMutex();

	//printString("\nTick: ");
//	printInt(localCounter);
//	printNewLine();

}

void keyboardHandler() {

    char c;
    c = KeyBuffer;
   	print("a pressed!\n", 11);	
    if(c == 'a') {
    	YKEventSet(charEvent, EVENT_A_KEY);

    }
    else if(c == 'b') YKEventSet(charEvent, EVENT_B_KEY);
    else if(c == 'c') YKEventSet(charEvent, EVENT_C_KEY);
    else if(c == 'd') YKEventSet(charEvent, EVENT_A_KEY | EVENT_B_KEY | EVENT_C_KEY);
    else if(c == '1') YKEventSet(numEvent, EVENT_1_KEY);
    else if(c == '2') YKEventSet(numEvent, EVENT_2_KEY);
    else if(c == '3') YKEventSet(numEvent, EVENT_3_KEY);
    else {
        print("\nKEYPRESS (", 11);
        printChar(c);
        print(") IGNORED\n", 10);
    }

}
