# 1 "myinth.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "myinth.c"
# 1 "clib.h" 1






void print(char *string, int length);
void printNewLine(void);
void printChar(char c);
void printString(char *string);


void printInt(int val);
void printLong(long val);
void printUInt(unsigned val);
void printULong(unsigned long val);


void printByte(char val);
void printWord(int val);
void printDWord(long val);


void exit(unsigned char code);


void signalEOI(void);
# 2 "myinth.c" 2
# 1 "yakk.h" 1



# 1 "yaku.h" 1
# 5 "yakk.h" 2
# 24 "yakk.h"
enum TaskState {T_BLOCKED, T_READY, T_RUNNING};
enum KernelState {K_BLOCKED, K_RUNNING};

typedef struct TCB {
 unsigned int eventMask;
 unsigned char waitMode;
 unsigned char priority;
 void* stackPointer;
 enum TaskState state;
 unsigned int delayCount;
 struct TCB* next;
 struct TCB* prev;
} TCB;

typedef struct TaskBlock {
 TCB TCBPool[10 +1];
 unsigned int nextFreeTCB;
} TaskBlock;

typedef struct PriorityQueue {
 TCB* head;
 TCB* tail;
 unsigned int size;
} PriorityQueue;

typedef struct DelayQueue {
 TCB* head;
 unsigned int size;
} DelayQueue;

typedef struct Semaphore {
 int value;
 PriorityQueue queue;
} YKSEM;

typedef struct SemBlock {
 YKSEM SemPool[10];
 unsigned int nextFreeSem;
} SemBlock;

typedef struct MessageQueue {
 void** messages;
 unsigned int currentSize;
 unsigned int maxSize;
 PriorityQueue queue;
} YKQ;

typedef struct MessageQueueBlock {
 YKQ QueuePool[10];
 unsigned int nextFreeQueue;
} MsgQueueBlock;

typedef struct Event {
 unsigned int mask;
 PriorityQueue queue;
} YKEVENT;

typedef struct EventBlock {
 YKEVENT EventPool[10];
 unsigned int newFreeEvent;
} EventBlock;


void YKInitialize(void);
void YKEnterMutex(void);
void YKExitMutex(void);
void YKIdleTask(void);
void YKNewTask(void (* task) (void), void *taskStack, unsigned char priority);
void YKDelayTask(unsigned count);
void YKEnterISR(void);
void YKExitISR(void);
unsigned int YKGetISRCallDepth(void);
void YKScheduler(void);
void YKDispatcher(TCB* readyTask);
void YKTickHandler(void);
YKSEM* YKSemCreate(int initialValue);
void YKSemPend(YKSEM* semaphore);
void YKSemPost(YKSEM* semaphore);
YKQ* YKQCreate(void** start, unsigned size);
void* YKQPend(YKQ* queue);
int YKQPost(YKQ* queue, void* msg);
YKEVENT* YKEventCreate(unsigned initialValue);
unsigned YKEventPend(YKEVENT* event, unsigned eventMask, int waitMode);
void YKEventSet(YKEVENT* event, unsigned eventMask);
void YKEventReset(YKEVENT* event, unsigned eventMask);
TCB* getNewTCB(void);
YKSEM* getNewSem(void);
YKQ* getNewQueue(void);
YKEVENT* getNewEvent(void);
void YKRun(void);

unsigned int getYKCtxSwCount();
unsigned int getYKIdelCount();
void setYKIdelCount(int value);
void setYKCtxSwCount(int value);
unsigned int getYKTickNum();
# 3 "myinth.c" 2
# 1 "DelayQueue.h" 1





void initializeDelayQueue(void);

void tickClock(void);

void insertDelayQueue(TCB* tcb);

void printDelayQueue(void);
# 4 "myinth.c" 2
# 1 "lab7defs.h" 1
# 15 "lab7defs.h"
extern YKEVENT *charEvent;
extern YKEVENT *numEvent;
# 5 "myinth.c" 2

extern unsigned int YKTickCounter;
extern char KeyBuffer;
extern YKEVENT* charEvent;
extern YKEVENT* numEvent;

void resetHandler() {
 exit(0);
}

void tickHandler() {

 unsigned int localCounter;

 tickClock();
 YKEnterMutex();
 localCounter = ++YKTickCounter;
 YKExitMutex();





}

void keyboardHandler() {

    char c;
    c = KeyBuffer;
    print("a pressed!\n", 11);
    if(c == 'a') {
     YKEventSet(charEvent, 0x1);

    }
    else if(c == 'b') YKEventSet(charEvent, 0x2);
    else if(c == 'c') YKEventSet(charEvent, 0x4);
    else if(c == 'd') YKEventSet(charEvent, 0x1 | 0x2 | 0x4);
    else if(c == '1') YKEventSet(numEvent, 0x1);
    else if(c == '2') YKEventSet(numEvent, 0x2);
    else if(c == '3') YKEventSet(numEvent, 0x4);
    else {
        print("\nKEYPRESS (", 11);
        printChar(c);
        print(") IGNORED\n", 10);
    }

}
