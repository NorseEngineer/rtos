# 1 "MessageQueue.c"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "MessageQueue.c"
# 1 "yakk.h" 1



# 1 "yaku.h" 1
# 5 "yakk.h" 2
# 24 "yakk.h"
enum TaskState {T_BLOCKED, T_READY, T_RUNNING};
enum KernelState {K_BLOCKED, K_RUNNING};

typedef struct TCB {
 unsigned int eventMask;
 unsigned char waitMode;
 unsigned char priority;
 void* stackPointer;
 enum TaskState state;
 unsigned int delayCount;
 struct TCB* next;
 struct TCB* prev;
} TCB;

typedef struct TaskBlock {
 TCB TCBPool[10 +1];
 unsigned int nextFreeTCB;
} TaskBlock;

typedef struct PriorityQueue {
 TCB* head;
 TCB* tail;
 unsigned int size;
} PriorityQueue;

typedef struct DelayQueue {
 TCB* head;
 unsigned int size;
} DelayQueue;

typedef struct Semaphore {
 int value;
 PriorityQueue queue;
} YKSEM;

typedef struct SemBlock {
 YKSEM SemPool[10];
 unsigned int nextFreeSem;
} SemBlock;

typedef struct MessageQueue {
 void** messages;
 unsigned int currentSize;
 unsigned int maxSize;
 PriorityQueue queue;
} YKQ;

typedef struct MessageQueueBlock {
 YKQ QueuePool[10];
 unsigned int nextFreeQueue;
} MsgQueueBlock;

typedef struct Event {
 unsigned int mask;
 PriorityQueue queue;
} YKEVENT;

typedef struct EventBlock {
 YKEVENT EventPool[10];
 unsigned int newFreeEvent;
} EventBlock;


void YKInitialize(void);
void YKEnterMutex(void);
void YKExitMutex(void);
void YKIdleTask(void);
void YKNewTask(void (* task) (void), void *taskStack, unsigned char priority);
void YKDelayTask(unsigned count);
void YKEnterISR(void);
void YKExitISR(void);
unsigned int YKGetISRCallDepth(void);
void YKScheduler(void);
void YKDispatcher(TCB* readyTask);
void YKTickHandler(void);
YKSEM* YKSemCreate(int initialValue);
void YKSemPend(YKSEM* semaphore);
void YKSemPost(YKSEM* semaphore);
YKQ* YKQCreate(void** start, unsigned size);
void* YKQPend(YKQ* queue);
int YKQPost(YKQ* queue, void* msg);
YKEVENT* YKEventCreate(unsigned initialValue);
unsigned YKEventPend(YKEVENT* event, unsigned eventMask, int waitMode);
void YKEventSet(YKEVENT* event, unsigned eventMask);
void YKEventReset(YKEVENT* event, unsigned eventMask);
TCB* getNewTCB(void);
YKSEM* getNewSem(void);
YKQ* getNewQueue(void);
YKEVENT* getNewEvent(void);
void YKRun(void);

unsigned int getYKCtxSwCount();
unsigned int getYKIdelCount();
void setYKIdelCount(int value);
void setYKCtxSwCount(int value);
unsigned int getYKTickNum();
# 2 "MessageQueue.c" 2
# 1 "clib.h" 1






void print(char *string, int length);
void printNewLine(void);
void printChar(char c);
void printString(char *string);


void printInt(int val);
void printLong(long val);
void printUInt(unsigned val);
void printULong(unsigned long val);


void printByte(char val);
void printWord(int val);
void printDWord(long val);


void exit(unsigned char code);


void signalEOI(void);
# 3 "MessageQueue.c" 2
# 1 "PriorityQueue.h" 1





void initializePriorityQueue(PriorityQueue* queue);

void insertPriorityQueue(PriorityQueue* queue, TCB* tcb);

TCB* peekPriorityQueue(PriorityQueue* queue);

TCB* removePriorityQueue(PriorityQueue* queue);

void printPriorityQueue(PriorityQueue* queue);
# 4 "MessageQueue.c" 2

extern PriorityQueue readyQueue;

YKQ* YKQCreate(void** start, unsigned size) {

 YKQ* newMessageQueue;

 newMessageQueue = getNewQueue();
 if (newMessageQueue == 0) exit(4);

 initializePriorityQueue(&(newMessageQueue->queue));
 newMessageQueue->messages = start;
 newMessageQueue->maxSize = size;
 newMessageQueue->currentSize = 0;

 return newMessageQueue;

}

void* YKQPend(YKQ* queue) {

 void* message;
 int i;
 TCB* runningTask;

 if (queue == 0) return 0;

 YKEnterMutex();
 if (*queue->messages == 0) {
  runningTask = removePriorityQueue(&readyQueue);
  runningTask->state = T_BLOCKED;
  insertPriorityQueue((&(queue->queue)), runningTask);
  YKExitMutex();
  asm("int 0x20");
 }

 message = queue->messages[0];
 for (i = 0; i < queue->currentSize-1; i++) {
  queue->messages[i] = queue->messages[i+1];
 }
 queue->currentSize--;
 queue->messages[queue->currentSize] = 0;

 YKExitMutex();
 return message;

}

int YKQPost(YKQ* queue, void* msg) {

 TCB* readyTask;
 int retVal;

 YKEnterMutex();
 if (queue->currentSize < queue->maxSize) {
  queue->messages[queue->currentSize] = msg;
  queue->currentSize++;
  retVal = 1;

 } else {
  retVal = 1;
 }

 readyTask = removePriorityQueue(&(queue->queue));
 if (readyTask == 0) {
  YKExitMutex();
  return retVal;
 } else {
  readyTask->state = T_READY;
  insertPriorityQueue(&readyQueue, readyTask);
  YKExitMutex();
  if (YKGetISRCallDepth() == 0) {
   asm("int 0x20");
  }
 }
 return retVal;

}
